from django.urls import path
from . import views

urlpatterns = [
    path('', views.cv, name='career_history'),
]
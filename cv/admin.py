from django.contrib import admin
from cv.models import Position, Category, Responsibility

class PositionAdmin(admin.ModelAdmin):
    pass

class CategoryAdmin(admin.ModelAdmin):
    pass

class ResponsibilityAdmin(admin.ModelAdmin):
    pass

admin.site.register(Position, PositionAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Responsibility, ResponsibilityAdmin)

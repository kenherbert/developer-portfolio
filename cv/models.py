from django.db import models

# Create your models here.

class Category(models.Model):
    name = models.CharField(max_length=20)
    show_as_filter = models.BooleanField(default=False)
    show_as_tag = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "categories"

class Position(models.Model):
    position = models.CharField(max_length=255)
    employer = models.CharField(max_length=255)
    start_date = models.DateField()
    end_date = models.DateField(null=True, blank=True)
    categories = models.ManyToManyField('Category', related_name='posts')
    displayed = models.BooleanField(default=False)

    def __str__(self):
        return self.position + ' - ' + self.employer

class Responsibility(models.Model):
    body = models.TextField()
    position = models.ForeignKey('Position', on_delete=models.CASCADE)

    def __str__(self):
        return self.body

    class Meta:
        verbose_name_plural = "responsibilities"

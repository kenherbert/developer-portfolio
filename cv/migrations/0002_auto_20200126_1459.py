# Generated by Django 2.2.6 on 2020-01-26 14:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cv', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='position',
            name='ended',
            field=models.CharField(max_length=10),
        ),
        migrations.AlterField(
            model_name='position',
            name='started',
            field=models.CharField(max_length=10),
        ),
    ]

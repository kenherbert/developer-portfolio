# Generated by Django 2.2.6 on 2020-01-26 21:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cv', '0006_auto_20200126_1756'),
    ]

    operations = [
        migrations.AddField(
            model_name='link',
            name='order',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
    ]

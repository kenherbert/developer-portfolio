from django.shortcuts import render
from django.http import Http404
from cv.models import Position, Responsibility, Category
from config.models import SiteConfiguration
CONFIG = SiteConfiguration.objects.get()

def cv(request):
    if CONFIG.display_cv is False:
        raise Http404

    filters = Category.objects.filter(show_as_filter=True)

    positions = Position.objects.filter(displayed=True).order_by('-start_date', '-end_date')

    for position in positions:
        position.responsibilities = Responsibility.objects.filter(position=position)
        position.tags = position.categories.filter(show_as_tag=True)

    context = {
        'positions': positions,
        'filters': filters,
        'title': 'Career History',
        'page_description': 'My career history in software development, social media, management, and service',
    }

    return render(request, "cv.html", context)

# Developer Portfolio

## Installation

- Clone the repo in your usual manner.

- Create the following environment variables:
    - `DJANGO_SECRET_KEY` - anything you want - a long random string is recommended - this value should not be made available to the outside world in any way
    - `DJANGO_DEBUG` (optional, defaults to `False`) - An empty string `""` for production/release testing, `"True"` (or anything except an empty string) for dev setups
    - `DJANGO_ALLOWED_HOSTS` (optional, defaults to an empty list) - a space separated list of allowed domains from which the site can be accessed

These should be available to the project however you normally would for your operating system/shell/hosting provider. Note that the project will not start without `DJANGO_SECRET_KEY` being set!

- Install the dependencies:
`pip install django-solo`
`pip install django-adminrestrict`

- Run `python manage.py migrate` from the root of the project (or `npm run migrate` if you have npm).

- Set up a superuser as you normally would for a Django site using `python manage.py createsuperuser` from the root of the project.

- Log into the admin panel and in the Flat Pages app add a new entry for the URL `/`. The content is not important for now and you can edit it later.

- Start it up! If you have npm installed you can use the `npm start` command, or just the good old `python manage.py runserver` will do it too.


## Configuration

To include your own links without breaking the ability to update from git since you would end up with conflicts in `urls.py`, open up `additional_urls.bak.py` and follow the instructions there. You can add local urls exactly the same as you would in `urls.py`.

## Important

When running a migration on the database which includes adding a page behind a feature switch in Site Configuration you will need to comment out the feature switch in `/config/models.py`, run the migration, then uncomment the feature switch. If you don't you will get a huge amount of errors that are not exactly indicative of the problem.

## Dependencies

Python 3 (tested against 3.7.4 in development & 3.7.5 in production)

Django 2.2.6

[django-solo](https://github.com/lazybird/django-solo)

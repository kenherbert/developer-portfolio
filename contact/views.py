from django.shortcuts import render
from contact.models import Link
from django.http import Http404
from config.models import SiteConfiguration
config = SiteConfiguration.objects.get()

def contact(request):
    if config.display_contact is False:
        raise Http404

    links = Link.objects.filter(visible=True).order_by('stacked_order')

    context = {
        'links': links,
        'title': "Contact Me",
        'page_description': 'Connect with me on social media and other platforms',
    }

    return render(request, "contact.html", context)
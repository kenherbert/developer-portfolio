from django.db import models

# Create your models here.

class Link(models.Model):
    icon = models.CharField(max_length=255)
    site = models.CharField(max_length=255)
    URI = models.URLField()
    username = models.CharField(max_length=255)
    visible = models.BooleanField()
    stacked_order = models.IntegerField()
    column_order = models.IntegerField()

    def __str__(self):
        return 'Link: ' + self.site


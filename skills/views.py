from django.shortcuts import render, redirect
from skills.models import Skill, Category
from django.http import Http404
from config.models import SiteConfiguration
config = SiteConfiguration.objects.get()


def skills(request):
    if config.display_skills is False:
        raise Http404

    categories = Category.objects.filter(displayed=True).order_by('sort_order')

    for category in categories:
        category.skills = Skill.objects.filter(category__pk=category.pk, displayed=True).distinct().order_by('-current', 'name').prefetch_related('tags')

        for skill in category.skills:
            tags = {tag.name for tag in skill.tags.all() if tag.name is not ''} | {tag.aliases for tag in skill.tags.all() if tag.aliases is not ''} | {skill.name}
            skill.currentTags = ','.join(tags)
#            skill.currentTags = [skill.name]
#            tags.append([tag.name for tag in skill.tags.all()])
#            tags.append([tag.name for tag in skill.tags.all()])
#            skill.currentTags = f'{skill.name},' + (','.join([tag.name for tag in skill.tags.all()])) + (','.join([tag.aliases for tag in skill.tags.all()]))



    context = {
        'categories': categories,
        'title': 'Skills',
        'page_description': 'Web and software projects that I have worked on, either solo or as part of a team',
    }
    return render(request, 'skills.html', context)

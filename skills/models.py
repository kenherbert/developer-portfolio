from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=40, default='')
    displayed = models.BooleanField(default=True)
    sort_order = models.IntegerField(default=0)

    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name_plural = "categories"


class Tag(models.Model):
    name = models.CharField(max_length=40, default='')
    aliases = models.CharField(max_length=255, default='', blank=True)
    weight = models.IntegerField(default=1)

    def __str__(self):
        return f'{self.name} ({self.aliases})'


class Skill(models.Model):
    name = models.CharField(max_length=255, default='')
    experience = models.CharField(max_length=255, default='', blank=True)
    current = models.BooleanField(default=True)
    displayed = models.BooleanField(default=True)
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    tags = models.ManyToManyField(Tag)
#    tags = models.CharField(max_length=255, default='', blank=True)

    def __str__(self):
        return f'{self.name}'

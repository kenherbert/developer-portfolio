# Generated by Django 2.2.6 on 2022-06-13 14:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('skills', '0012_auto_20211005_1310'),
    ]

    operations = [
        migrations.RenameField(
            model_name='skill',
            old_name='time',
            new_name='experience',
        ),
    ]

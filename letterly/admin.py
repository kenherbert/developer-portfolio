from django.contrib import admin
from letterly.models import Room, User, Guess

class RoomAdmin(admin.ModelAdmin):
    readonly_fields = ('id', 'date_generated',)
    pass

class UserAdmin(admin.ModelAdmin):
    pass

class GuessAdmin(admin.ModelAdmin):
    pass

admin.site.register(Room, RoomAdmin)
admin.site.register(User, UserAdmin)
admin.site.register(Guess, GuessAdmin)

import json
import requests
from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from letterly.models import Room
#from config.models import SiteConfiguration
from django.views.decorators.csrf import csrf_exempt


def generate_word_for_room(uuid):
    random_word_api = 'https://random-word-api.herokuapp.com/word?length=5&number=20'
    check_word_api = 'https://api.dictionaryapi.dev/api/v2/entries/en/'

    room = Room.objects.get(id=uuid)
    result = False

    try:
        random_word_response = requests.get(random_word_api)

        if not random_word_response:
            print('Can\'t retrieve random words')
        else:
            if random_word_response:
                word_api_data = random_word_response.json()

                for word in word_api_data:
                    try:
                        check_word_api_call = check_word_api + word
                        check_word_response = requests.get(check_word_api_call)

                        if check_word_response.status_code == 200 or check_word_response.status_code == 304:
                            room.word = word
                            room.save()
                            result = True

                            break
                    except Exception as error:
                        print('checkWordAPI call failed ' + error)

    except:
        print('randomWordAPI call failed')

    return result


@csrf_exempt
def get_daily_room_id(request):
    response = JsonResponse({
        'result': True,
        'room': 'eea736c9-5ee0-42ac-b59c-37f712645cbd'
    })

    response["Access-Control-Allow-Origin"] = "*"

    return response


@csrf_exempt
def get_new_room_id(request):
    result = False

    room = Room.objects.create(word='first')
    result = generate_word_for_room(room.id)

    response = JsonResponse({
        'result': result,
        'room': room.id
    })

    response["Access-Control-Allow-Origin"] = "*"

    return response


@csrf_exempt
def regenerate_word(request):
    print(request.body)
    body = json.loads(request.body.decode('utf-8'))

    response = JsonResponse({
        'result': generate_word_for_room(body['room'])
    })

    response["Access-Control-Allow-Origin"] = "*"

    return response


@csrf_exempt
def check_daily_word(request):
    body = json.loads(request.body.decode('utf-8'))
    room = get_object_or_404(Room, id=body['room'])
    base_word = room.word
    user_word = body['word']
    word_length = len(base_word)

    result = [0 for i in range(word_length)]

    for i in range(0, word_length):
        if user_word[i] == base_word[i]:
            result[i] = 2
            base_word = base_word[:i] + '*' + base_word[i + 1:]
            user_word = user_word[:i] + '*' + user_word[i + 1:]

    for i in range(0, word_length):
        found_index = base_word.find(user_word[i])

        if user_word[i] != "*" and found_index >= 0:
            result[i] = 1
            base_word = base_word[:found_index] + '*' + base_word[found_index + 1:]
            user_word = user_word[:i] + '*' + user_word[i + 1:]

    response = JsonResponse({
        'result': result
    })

    response["Access-Control-Allow-Origin"] = "*"

    return response

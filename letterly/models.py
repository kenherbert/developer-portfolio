import uuid
from django.db import models

# Create your models here.

class Room(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    word = models.CharField(max_length=20)
    date_generated = models.DateTimeField(auto_now=True)

#    def __str__(self):
#        return 'Category: ' + self.name

#    class Meta:
#        verbose_name_plural = "categories"


class User(models.Model):
    ip_address = models.GenericIPAddressField()
    room = models.ForeignKey('Room', on_delete=models.CASCADE)


class Guess(models.Model):
    guess = models.CharField(max_length=20)
    result = models.CharField(max_length=20)
    created_on = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey('User', on_delete=models.CASCADE)

#    def __str__(self):
#        return 'Post: ' + self.title

#    def get_absolute_url(self):
#        return reverse('blog_detail', kwargs={'slug': self.slug})
    class Meta:
        verbose_name_plural = "guesses"

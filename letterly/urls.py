from django.urls import path
from . import views

urlpatterns = [
    path(r'', views.check_daily_word, name='check_daily_word'),
    path('get-new-room-id', views.get_new_room_id, name='get_new_room_id'),
    path('get-daily-room-id', views.get_daily_room_id, name='get_daily_room_id'),
    path('generate-new-word', views.regenerate_word, name='generate_new_word'),
]

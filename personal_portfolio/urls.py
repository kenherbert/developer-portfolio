"""personal_portfolio URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import os.path
from pathlib import Path
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.contrib.flatpages import views
from personal_portfolio import settings


urlpatterns = [
    path(r'', views.flatpage, {'url': ''}, name='homepage'),
    path('about/', views.flatpage, {'url': '/about/'}, name='about'),
    path('privacy/', views.flatpage, {'url': '/privacy/'}, name='privacy'),
    path('admin/', admin.site.urls),
    path('projects/', include('projects.urls')),
    path('software/', include('software.urls')),
    path('blog/', include('blog.urls')),
    path('skills/', include('skills.urls')),
    path('career-history/', include("cv.urls")),
    path('contact/', include("contact.urls")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

#import additional urls, if the file exists and the urlpatterns member is defined
additional_urls_file = Path(os.path.join(settings.BASE_DIR, 'additional_urls.py'))

if additional_urls_file.is_file():
    import additional_urls

    if hasattr(additional_urls, 'urlpatterns'):
        urlpatterns = urlpatterns + additional_urls.urlpatterns

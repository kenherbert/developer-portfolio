from django.db import models
from solo.models import SingletonModel

class SiteConfiguration(SingletonModel):
    site_name = models.CharField(max_length=255, default='Developer Portfolio')
    display_blog = models.BooleanField(default=False)
    display_blog_categories = models.BooleanField(default=False, help_text="Has no effect if blog page is hidden")
    display_contact = models.BooleanField(default=False)
    display_cv = models.BooleanField(default=False)
    display_projects = models.BooleanField(default=False)
    display_software = models.BooleanField(default=False)
    display_skills = models.BooleanField(default=False)

    def __str__(self):
        return "Site Configuration"

    class Meta:
        verbose_name = "Site Configuration"

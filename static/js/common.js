function setDarkMode() {
    document.getElementById('dark-mode').checked = localStorage.getItem('dark-mode') === 'true';
}


function toggleDarkMode() {
    console.log(this.checked)
    localStorage.setItem('dark-mode', this.checked.toString());

    updateCurrentDisplayMode();
}


function updateCurrentDisplayMode() {
    if(localStorage.getItem('dark-mode') === 'true') {
        document.getElementsByTagName('body')[0].classList.add('dark-mode');
    } else {
        document.getElementsByTagName('body')[0].classList.remove('dark-mode');
    }
}

document.addEventListener("DOMContentLoaded", function() {
    document.getElementById('dark-mode').onclick = toggleDarkMode;
    setDarkMode();
    updateCurrentDisplayMode();
});

# Generated by Django 2.2.6 on 2020-07-09 00:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0010_auto_20200407_1519'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='slug',
            field=models.SlugField(default='', max_length=100),
        ),
    ]

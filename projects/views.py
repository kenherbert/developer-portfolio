from django.shortcuts import render, redirect
from projects.models import Project
from django.http import Http404
from config.models import SiteConfiguration
config = SiteConfiguration.objects.get()

def project_index(request):
    if config.display_projects is False:
        raise Http404

    projects = Project.objects.all().order_by('sort_order')
    current_projects = []
    obsolete_projects = []

    for project in projects:
        if project.displayed == True:
            if project.obsolete == True:
                obsolete_projects.append(project)
            else:
                current_projects.append(project)

    context = {
        'current_projects': current_projects,
        'obsolete_projects': obsolete_projects,
        'title': 'Projects',
        'page_description': 'Web and software projects that I have worked on, either solo or as part of a team',
    }
    return render(request, 'project_index.html', context)


def project_detail(request, slug):
    if config.display_projects is False:
        raise Http404

    try:
        project = Project.objects.get(slug=slug)
    except Project.DoesNotExist:
        raise Http404

    if project.displayed == False:
        return redirect('project_index')

    context = {
        'project': project,
        'title': project.title,
        #TODO: Add a page description field to project entries
        'page_description': '',
    }
    return render(request, 'project_detail.html', context)

from django.db import models
from django.utils.html import format_html
from django.urls import reverse


class Project(models.Model):
    title = models.CharField(max_length=100, default='')
    slug = models.SlugField(max_length=100, default='', unique=True)
    description = models.TextField(default='')
    timeline = models.TextField(default='')
    technology = models.CharField(max_length=200, default='')
    link = models.URLField(default='')
    link_rel_attribute = models.CharField(max_length=100, default='external nofollow noopener')
    image = models.ImageField(upload_to='img/', default='')
    displayed = models.BooleanField(default=False)
    obsolete = models.BooleanField(default=False)
    sort_order = models.IntegerField(default=0)

    @property
    def formatted_url(self):
        return format_html('<a href="{}" target="_blank" rel="{}">{}</a>', self.link, self.link_rel_attribute, self.link)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('project_detail', kwargs={'slug': self.slug})

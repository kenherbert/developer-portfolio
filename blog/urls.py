from django.urls import path
from . import views
from .feeds import PostFeed

urlpatterns = [
    path(r'', views.blog_index, name='blog_index'),
    path('feed/', PostFeed(), name='post-feed-all'),
    path('<slug:slug>/', views.blog_detail, name='blog_detail'),
    path('category/<category>/', views.blog_category, name='blog_category'),
]
from django.contrib.syndication.views import Feed
from .models import Post
from django.urls import reverse

class PostFeed(Feed):
    def get_object(self, request):
        return None

    def items(self):
        items = Post.objects.order_by("-created_on")
        return items

    def description(self):
        return "The latest updates from developer Ken Herbert"

    def link(self):
        return reverse("blog_index")

    def title(self):
        return "Blog posts from kenherbert.dev"

    def item_pubdate(self, item):
        return item.created_on

    item_guid_is_permalink = True
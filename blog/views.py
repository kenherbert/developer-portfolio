from django.shortcuts import render
from blog.models import Post, Comment
from blog.forms import CommentForm
from django.http import Http404
from config.models import SiteConfiguration
config = SiteConfiguration.objects.get()

def blog_index(request):
    if config.display_blog is False:
        raise Http404

    posts = Post.objects.all().order_by('-created_on')
    context = {
        'posts': posts,
        'title': 'Blog',
        'page_description': 'Latest news and happenings in my little corner of web and software development',
    }

    return render(request, 'blog_index.html', context)

def blog_category(request, category):
    if config.display_blog is False or config.display_blog_categories is False:
        raise Http404

    posts = Post.objects.filter(
        categories__name__contains=category
    ).order_by('-created_on')
    context = {
        'category': category,
        'posts': posts,
        'title': category,
        'page_description': f'My posts tagged {category}',
    }

    return render(request, 'blog_category.html', context)

def blog_detail(request, slug):
    if config.display_blog is False:
        raise Http404

    try:
        post = Post.objects.get(slug=slug)
    except Post.DoesNotExist:
        raise Http404

    form = CommentForm()
    if request.method == 'POST':
        form = CommentForm(request.POST)

#TODO: Handle error if the form was invalid
#TODO: Strip links from author and body field (they are rendered as plain text right now)
        if form.is_valid():
            comment = Comment(
                author = form.cleaned_data['author'],
                body = form.cleaned_data['body'],
                post = post
            )
            comment.save()

    comments = Comment.objects.filter(post=post)
    context = {
        'post': post,
        'comments': comments,
        'form': form,
        'title': post.title,
        #TODO: Add a page description to blog post entries
        'page_description': '',
    }

    return render(request, 'blog_detail.html', context)

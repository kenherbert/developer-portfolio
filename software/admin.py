from django.contrib import admin
from software.models import Software, Release

class SoftwareAdmin(admin.ModelAdmin):
    pass

class ReleaseAdmin(admin.ModelAdmin):
    pass

admin.site.register(Software, SoftwareAdmin)
admin.site.register(Release, ReleaseAdmin)

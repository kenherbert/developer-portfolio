from django.urls import path
from . import views
from .feeds import ReleaseFeed

urlpatterns = [
    path('', views.software_index, name='software_index'),
    path('get-latest-versions/', views.version_check, name='version-check'),
    path('<slug:slug>/', views.software_detail, name='software_detail'),
    path('<slug:slug>/feed/', ReleaseFeed(), name='release-feed'),
    path('metadata/<int:pk>/<str:download_type>/', views.submit_metadata, name='software_metadata'),
]

import os

from django.shortcuts import render, redirect
from django.http import Http404, HttpResponse, JsonResponse
from personal_portfolio.settings import MEDIA_ROOT
from software.models import Software, Release

from config.models import SiteConfiguration
config = SiteConfiguration.objects.get()


def naturalsize(value, binary=False, gnu=False, output_format="%.1f"):
    suffixes = {
        "decimal": ("kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"),
        "binary": ("KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"),
        "gnu": "KMGTPEZY",
    }
    """Format a number of bytes like a human readable filesize (e.g. 10 kB).
    By default, decimal suffixes (kB, MB) are used.
    Non-GNU modes are compatible with jinja2's `filesizeformat` filter.
    Examples:
        ```pycon
        >>> naturalsize(3000000)
        '3.0 MB'
        >>> naturalsize(300, False, True)
        '300B'
        >>> naturalsize(3000, False, True)
        '2.9K'
        >>> naturalsize(3000, False, True, "%.3f")
        '2.930K'
        >>> naturalsize(3000, True)
        '2.9 KiB'
        ```
    Args:
        value (int, float, str): Integer to convert.
        binary (bool): If `True`, uses binary suffixes (KiB, MiB) with base
            2<sup>10</sup> instead of 10<sup>3</sup>.
        gnu (bool): If `True`, the binary argument is ignored and GNU-style
            (`ls -sh` style) prefixes are used (K, M) with the 2**10 definition.
        format (str): Custom formatter.
    Returns:
        str: Human readable representation of a filesize.
    """
    if gnu:
        suffix = suffixes["gnu"]
    elif binary:
        suffix = suffixes["binary"]
    else:
        suffix = suffixes["decimal"]

    base = 1024 if (gnu or binary) else 1000
    bytes_value = float(value)
    abs_bytes = abs(bytes_value)

    if abs_bytes == 1 and not gnu:
        return "%d Byte" % bytes_value
    elif abs_bytes < base and not gnu:
        return "%d Bytes" % bytes_value
    elif abs_bytes < base and gnu:
        return "%dB" % bytes_value

    for i, s in enumerate(suffix):
        unit = base ** (i + 2)
        if abs_bytes < unit and not gnu:
            return (output_format + " %s") % ((base * bytes_value / unit), s)
        elif abs_bytes < unit and gnu:
            return (output_format + "%s") % ((base * bytes_value / unit), s)
    if gnu:
        return (output_format + "%s") % ((base * bytes_value / unit), s)
    return (output_format + " %s") % ((base * bytes_value / unit), s)


def software_index(request):
    if config.display_software is False:
        raise Http404

    software = Software.objects.all()

    for app in software:
        releases = Release.objects.filter(software=app.pk).filter(released=True)

        for release in releases:
            if release.windows_file_location:
                app.available_for_windows = True
                app.has_releases = True

            if release.mac_file_location:
                app.available_for_mac = True
                app.has_releases = True

            if release.portableapps_file_location or release.portableapps_official_release_location:
                app.available_for_portableapps = True
                app.has_releases = True

    context = {
        'software': software,
        'title': 'Software',
        'page_description': 'My available software',
    }
    return render(request, 'software_index.html', context)


def software_detail(request, slug):
    if config.display_software is False:
        raise Http404

    app = Software.objects.get(slug=slug)
    app.licence_display = app.get_licence_display()

    if not app.displayed:
        return redirect('software_index')

    all_releases = Release.objects.filter(software=app).filter(released=True).order_by('-release_date')

    if len(all_releases) == 0:
        return  redirect('software_index')

    show_old_releases = False
    first = True

    for release in all_releases:
        release.formatted_release_date = release.release_date.strftime('%b %d %Y')
        win_file = release.windows_file_location
        papps_file = release.portableapps_file_location
        mac_file = release.mac_file_location

        release.windows_downloads_total = release.windows_downloads_base + release.windows_downloads
        release.mac_downloads_total = release.mac_downloads_base + release.mac_downloads
        release.portableapps_downloads_total = release.portableapps_downloads_base + release.portableapps_downloads
        release.downloads_total = release.windows_downloads_total + release.mac_downloads_total + release.portableapps_downloads_total

        if win_file:
            win_file_path = MEDIA_ROOT + '/' + str(win_file)
            release.windows_filesize = naturalsize(os.path.getsize(win_file_path))
        else:
            release.windows_filesize = 0

        if papps_file:
            papps_file_path = MEDIA_ROOT + '/' + str(papps_file)
            release.portableapps_filesize = naturalsize(os.path.getsize(papps_file_path))
        else:
            release.portableapps_filesize = 0

        if mac_file:
            mac_file_path = MEDIA_ROOT + '/' + str(mac_file)
            release.mac_filesize = naturalsize(os.path.getsize(mac_file_path))
        else:
            release.mac_filesize = 0

        if not first and not show_old_releases and (release.windows_filesize != 0 or release.portableapps_filesize != 0 or release.mac_filesize != 0):
            show_old_releases = True

        first = False

    latest_release = all_releases[0]
    releases = all_releases[1:]

    context = {
        'app': app,
        'latest_release': latest_release,
        'releases': releases,
        'show_old_releases': show_old_releases,
        'title': app.name,
        #TODO: Add a page description field to entries
        'page_description': '',
    }
    return render(request, 'software_detail.html', context)


def submit_metadata(request, pk, download_type):
    if config.display_software is False:
        raise Http404

    file = Release.objects.get(pk=pk)

    if download_type == 'MAC':
        file.mac_downloads += 1

    if download_type == 'WIN':
        file.windows_downloads += 1

    if download_type == 'PAP':
        file.portableapps_downloads += 1

    file.save()
    return HttpResponse("metadata submitted")


def version_check(request):
    software = Software.objects.all()

    data = {}

    for app in software:
        releases = Release.objects.filter(software=app.pk).filter(released=True).order_by('-release_date')
        data[app.slug] = {}

        for release in releases:

            if 'windows' not in data[app.slug] and release.windows_file_location:
                data[app.slug]['windows'] = release.version

            if 'mac' not in data[app.slug] and release.mac_file_location:
                data[app.slug]['mac'] = release.version

    return JsonResponse(data)

from django.contrib.syndication.views import Feed
from .models import Release, Software
from django.urls import reverse

class ReleaseFeed(Feed):
    def get_object(self, request, slug):
        if slug:
            app = Software.objects.get(slug=slug)

            if(app.displayed):
                return app

        return None

    def items(self, app):
        items = Release.objects.filter(software=app, released=True).order_by("-release_date")

        return items

    def description(self, app):
        return "The latest releases for" + app.name + "from kenherbert.dev"

    def link(self, app):
        return reverse("release-feed", kwargs={"slug": app.slug})

    def title(self, app):
        return "Release history for " + app.name

    def item_link(self, item):
        return reverse("software_detail", kwargs={"slug": item.software.slug})

    def item_pubdate(self, item):
        return item.release_date

    def item_guid(self, item):
        return str(item)

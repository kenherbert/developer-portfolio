# Generated by Django 2.2.6 on 2019-10-24 20:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Software',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=100)),
                ('slug', models.SlugField(default='', max_length=100)),
                ('description', models.TextField(default='')),
                ('image', models.ImageField(default='', upload_to='img/')),
                ('displayed', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Release',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('version', models.CharField(default='0.0.1', max_length=10)),
                ('file_location', models.FileField(upload_to='uploads/')),
                ('release_notes', models.TextField(default='')),
                ('release_date', models.DateTimeField(auto_now_add=True)),
                ('technology', models.CharField(default='', max_length=100)),
                ('released', models.BooleanField(default=False)),
                ('software', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='software.Software')),
            ],
        ),
    ]

#TODO: Add related file downloads (eg. BitMaker profile)
from django.db import models


class Software(models.Model):
    GPL3 = 'GPL3'
    MIT = 'MIT'
    LICENCE_CHOICES = [
        (GPL3, 'GPL v3'),
        (MIT, 'MIT'),
    ]

    name = models.CharField(max_length=100, default='')
    slug = models.SlugField(max_length=100, default='')
    image = models.ImageField(upload_to='img/', default='', blank=True)
    description = models.TextField(default='')
    licence = models.CharField(max_length=4, choices=LICENCE_CHOICES, default=GPL3)
    source_link = models.URLField(default='')
    command_line_parameters = models.TextField(default='', blank=True)
    displayed = models.BooleanField(default=False)
    hide_cover_in_details = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "apps"

# Helper to generate the file path based on parent software's slug
def content_file_name(instance, filename):
    return '/'.join(['software', instance.software.slug, filename])


class Release(models.Model):
    WINDOWS = 'WIN'
    MAC = 'MAC'
    APP = 'PAP'
    PLATFORM_CHOICES = [
        (WINDOWS, 'Windows'),
        (MAC, 'OS X'),
        (APP, 'PortableApps'),
    ]

    version = models.CharField(max_length=10, default='0.0.1')
    windows_file_location = models.FileField(upload_to=content_file_name, null=True, blank=True)
    windows_downloads = models.IntegerField(default=0)
    windows_downloads_base = models.IntegerField(default=0)
    mac_file_location = models.FileField(upload_to=content_file_name, null=True, blank=True)
    mac_downloads = models.IntegerField(default=0)
    mac_downloads_base = models.IntegerField(default=0)
    portableapps_official_release_location = models.CharField(max_length=100, default='', blank=True)
    portableapps_file_location = models.FileField(upload_to=content_file_name, null=True, blank=True)
    portableapps_downloads = models.IntegerField(default=0)
    portableapps_downloads_base = models.IntegerField(default=0)
    release_notes = models.TextField(default='', blank=True)
    release_date = models.DateTimeField()
    released = models.BooleanField(default=False)
    software = models.ForeignKey('Software', on_delete=models.CASCADE)

    def __str__(self):
        return self.software.name + ' v' + self.version
